class User < ActiveRecord::Base

  acts_as_voter
  has_many :pins, dependent: :destroy

  def self.sign_in_from_omniauth(auth)
    find_by(provider: auth['provider'], uid: auth['uid']) || create_user_from_omniauth(auth)
  end

  def self.create_user_from_omniauth(auth)
    create(
      provider: auth['provider'],
      uid: auth['uid'],
      name: auth['info']['name'],
      avatar: auth['info']['image'].sub("_normal", "")
      # image: auth['info']['image'].sub("_normal", "")
      )
  end

  def self.search(search)
    where("name LIKE ?", "%#{search}%") 
  end

  # Prettifies the URLs - some
  def to_param
    "#{id} #{name[0,10]}".parameterize
  end
end