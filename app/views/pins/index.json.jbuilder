json.array!(@pins) do |pin|
  json.extract! pin, :id, :content
  json.url pin_url(pin, format: :json)
end
