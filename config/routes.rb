Trenda::Application.routes.draw do

  root 'pins#index'
  match '/auth/:provider/callback', to: 'sessions#create', via: 'get', as: 'login'
  match '/auth/failure', to: redirect('/'), via: 'get'
  delete 'logout' => 'sessions#destroy'
  get 'signup' => 'users#new'
  get 'about' => 'pages#about'
  get 'contact' => 'pages#contact'
  get 'faq' => 'pages#faq'
  get 'tos' => 'pages#tos'
  get 'privacy' => 'pages#privacy'
  post 'signup' => 'users#create'
  resources :users
  resources :pins do
    member do
      put "like" => "pins#like"
      put "dislike" => "pins#dislike"
    end
  end
end