class CreatePins < ActiveRecord::Migration

  # def self.up
  #   create_table :pins do |t|
  #     t.string :title
  #     t.text :content
  #     t.string :picture
  #     t.references :user, foreign_key: true

  #     t.timestamps
  #   end
  #   add_index :pins, :title
  #   add_index :pins, :content
  # end

  def self.down
    drop_table :pins
  end
end
