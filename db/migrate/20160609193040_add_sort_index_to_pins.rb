class AddSortIndexToPins < ActiveRecord::Migration

  def self.up
  	add_index :pins, [:user_id, :cached_votes_score]
  end

  def self.down
  	remove_index :pins, [:user_id, :cached_votes_score]
  end
end