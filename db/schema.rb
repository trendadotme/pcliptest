# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160615034949) do

  create_table "pins", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.string   "picture"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cached_votes_total",      default: 0
    t.integer  "cached_votes_score",      default: 0
    t.integer  "cached_votes_up",         default: 0
    t.integer  "cached_votes_down",       default: 0
    t.integer  "cached_weighted_score",   default: 0
    t.integer  "cached_weighted_total",   default: 0
    t.float    "cached_weighted_average", default: 0.0
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  add_index "pins", ["cached_votes_down"], name: "index_pins_on_cached_votes_down"
  add_index "pins", ["cached_votes_score"], name: "index_pins_on_cached_votes_score"
  add_index "pins", ["cached_votes_total"], name: "index_pins_on_cached_votes_total"
  add_index "pins", ["cached_votes_up"], name: "index_pins_on_cached_votes_up"
  add_index "pins", ["cached_weighted_average"], name: "index_pins_on_cached_weighted_average"
  add_index "pins", ["cached_weighted_score"], name: "index_pins_on_cached_weighted_score"
  add_index "pins", ["cached_weighted_total"], name: "index_pins_on_cached_weighted_total"
  add_index "pins", ["content"], name: "index_pins_on_content"
  add_index "pins", ["title"], name: "index_pins_on_title"
  add_index "pins", ["user_id", "cached_votes_score"], name: "index_pins_on_user_id_and_cached_votes_score"

  create_table "users", force: :cascade do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "avatar"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["name"], name: "index_users_on_name"

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"

end
